# sh2ezo.utils.AsyncInitializer

.NET library that can be used for asynchronous initialization of services while using  Microsoft.Extensions.DependencyInjection. Main idea was taken from [here](https://www.thomaslevesque.com/2018/09/25/asynchronous-initialization-in-asp-net-core-revisited/).

## Usage
``` csharp
public class MyAsyncResource : IAsyncInitializer
{
    // some code 

    public async Task InitializeAsync(CancellationToken cancellationToken)
    {
        // some initialization code
    }
}

public class MyResource
{
    public string Data { get; private set; } // some data from the internet

    public MyResource(string data) {
        Data = data;
    }

    // some code
}

public class MyResourceAsyncFactory : AsyncResourceFactory<MyResource>
{
    private HttpClient _client;

    // some code

    protected override async Task<MyResource> GetResourceAsync(CancellationToken cancellationToken) 
    {
        var data = await _client.GetStringAsync("http://example.com");

        return new MyResource(data);
    }
}

public class Startup
{
    // some code

    private void ConfigureServices(IServiceCollection services) 
    {
        services.AddAsyncResource<MyAsyncResource>();
        services.AddAsyncResourceWithFactory<MyResource, MyResourceAsyncFactory>();
    }
}
```