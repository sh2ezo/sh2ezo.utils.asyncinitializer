﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace sh2ezo.utils.AsyncInitializer
{
    public abstract class AsyncResourceFactory<T> : IAsyncInitializer
    {
        private bool _initialized;
        private T _resource;
        public T Resource => _initialized ? _resource : throw new NotInitializedException(this);

        public async Task InitializeAsync(CancellationToken cancellationToken)
        {
            if (_initialized)
            {
                throw new InvalidOperationException("already initialized");
            }

            _resource = await GetResourceAsync(cancellationToken).ConfigureAwait(false);
            _initialized = true;
        }

        protected abstract Task<T> GetResourceAsync(CancellationToken cancellationToken);
    }
}
