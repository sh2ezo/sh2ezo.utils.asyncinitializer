﻿using sh2ezo.utils.AsyncInitializer;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceProviderExtensions
    {
        /// <summary>
        /// Performs asynchronous initialization of services which implements <see cref="IAsyncInitializer"/>
        /// </summary>
        public static async Task InitializeAsyncResources(this IServiceProvider serviceProvider, CancellationToken cancellationToken)
        {
            var notInitializedServices = new Queue<NotInitializedService>(serviceProvider.GetServices<NotInitializedService>());

            while(notInitializedServices.Count > 0)
            {
                int prevCount = notInitializedServices.Count;

                for (int i = 0; i < prevCount; i += 1)
                {
                    var serviceInfo = notInitializedServices.Dequeue();

                    // service cannot be initialized twice
                    if (serviceInfo.Initialized)
                    {
                        throw new InvalidOperationException("attempt to initialize " + serviceInfo.ServiceType.FullName + " twice");
                    }

                    try
                    {
                        // get initializer interface of service
                        var service = serviceProvider.GetRequiredService(serviceInfo.ServiceType) as IAsyncInitializer;

                        // if for some reason service does not implement needed interface then throw exception
                        if(service == null)
                        {
                            throw new InvalidOperationException(service.GetType().FullName + " is not subclass of " + typeof(IAsyncInitializer).FullName);
                        }

                        // initialize
                        await service.InitializeAsync(cancellationToken).ConfigureAwait(false);

                        // mark as initialized
                        serviceInfo.Initialized = true;
                    }
                    catch(NotInitializedException) // add service back to queue if it has dependencies
                    {
                        notInitializedServices.Enqueue(serviceInfo);
                    }
                }

                // if queue contains the same number of items as before, then we have unresolveable dependencies
                if(prevCount == notInitializedServices.Count)
                {
                    throw new InvalidOperationException(notInitializedServices.Dequeue().GetType().FullName + " cannot be initialized");
                }
            }
        }
    }
}
