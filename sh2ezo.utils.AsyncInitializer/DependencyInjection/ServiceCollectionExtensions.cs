﻿using sh2ezo.utils.AsyncInitializer;
using System;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Adds a singleton service of <typeparamref name="TService"/> which is initialized asynchronously
        /// </summary>
        public static IServiceCollection AddAsyncResource<TService>(this IServiceCollection services) where TService : class, IAsyncInitializer
        {
            services.AddSingleton<TService>();
            services.AddSingleton<IAsyncInitializer, TService>(sp => sp.GetRequiredService<TService>());
            services.AddSingleton(new NotInitializedService(typeof(TService)));

            return services;
        }

        /// <summary>
        /// Adds a singleton service of <typeparamref name="TService"/> which is initialized asynchronously
        /// </summary>
        public static IServiceCollection AddAsyncResource<TService>(this IServiceCollection services, Func<IServiceProvider, TService> factory) where TService : class, IAsyncInitializer
        {
            services.AddSingleton(factory);
            services.AddSingleton<IAsyncInitializer, TService>(sp => sp.GetRequiredService<TService>());
            services.AddSingleton(new NotInitializedService(typeof(TService)));

            return services;
        }

        /// <summary>
        /// Adds a singleton service of <typeparamref name="TResource"/> which is returned by <see cref="AsyncResourceSource"/>
        /// </summary>
        public static IServiceCollection AddAsyncResourceWithFactory<TResource, TSource>(this IServiceCollection services) 
            where TResource : class
            where TSource : AsyncResourceFactory<TResource>
        {
            services.AddAsyncResource<TSource>();
            services.AddSingleton(sp => sp.GetRequiredService<TSource>().Resource);

            return services;
        }

        /// <summary>
        /// Adds a singleton service of <typeparamref name="TResource"/> which is returned by <typeparamref name="TSource"/>
        /// </summary>
        public static IServiceCollection AddAsyncResourceWithFactory<TResource>(this IServiceCollection services, Func<IServiceProvider, AsyncResourceFactory<TResource>> sourceFactory)
            where TResource : class
        {
            services.AddAsyncResource(sourceFactory);
            services.AddSingleton(sp => sp.GetRequiredService<AsyncResourceFactory<TResource>>().Resource);

            return services;
        }
    }
}
