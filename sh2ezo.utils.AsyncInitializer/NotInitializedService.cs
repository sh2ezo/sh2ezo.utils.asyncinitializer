﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sh2ezo.utils.AsyncInitializer
{
    internal class NotInitializedService
    {
        public Type ServiceType { get; }

        public bool Initialized { get; set; }

        public NotInitializedService(Type serviceType)
        {
            ServiceType = serviceType;
        }
    }
}
