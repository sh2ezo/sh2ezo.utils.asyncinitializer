﻿using System;

namespace sh2ezo.utils.AsyncInitializer
{
    public class NotInitializedException : Exception
    {
        public IAsyncInitializer Resource { get; private set; }

        public NotInitializedException(IAsyncInitializer resource)
        {
            Resource = resource;
        }
    }
}
