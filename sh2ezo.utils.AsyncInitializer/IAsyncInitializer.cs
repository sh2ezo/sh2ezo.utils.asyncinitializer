﻿using System.Threading;
using System.Threading.Tasks;

namespace sh2ezo.utils.AsyncInitializer
{
    /// <summary>
    /// Mark your class as needed the asynchronous initialization.
    /// Derived classes should throw the <see cref="NotInitializedException"/> 
    /// if someone attempts use them before initialization performed.
    /// </summary>
    public interface IAsyncInitializer
    {
        Task InitializeAsync(CancellationToken cancellationToken);
    }
}
